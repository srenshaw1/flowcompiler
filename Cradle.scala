
object Cradle {
	val TAB = '\t'
	val CR  = '\r'
	val LF  = '\n'

	var Look   = '\0'

	val ST = Array.fill(('Z' - 'A') + 1)(' ')
	val Params = Array.fill(('Z' - 'A') + 1)(0)
	
	var NumParams = 0

	def GetChar {
		Look = Console.in.read.toChar
	}

	def Error(s: String) {
		println
		println("\nError: " + s + ".")
	}

	def Abort(s: String) {
		Error(s)
		sys.exit
	}

	def Expected(s: String) {
		Abort(s + " Expected")
	}

	def Undefined(s: String) {
		Abort("Undefined Identifier " + s)
	}

	def Duplicate(s: String) {
		Abort("Duplicate Identifier " + s)
	}

	def TypeOf(c: Char): Char = ST(c.toUpper - 'A')

	def InTable(c: Char): Boolean = ST(c.toUpper - 'A') != ' '

	def AddEntry(Name: Char, T: Char) {
		if (InTable(Name)) {
			Duplicate(Name.toString)
		}
		ST(Name.toUpper - 'A') = T
	}

	def CheckVar(Name: Char) {
		if (!InTable(Name)) {
			Undefined(Name.toString)
		}
		if (TypeOf(Name) != 'v') {
			Abort(Name + " is not a variable")
		}
	}

	def IsAlpha(c: Char): Boolean = c.isLetter

	def IsDigit(c: Char): Boolean = c.isDigit

	def IsAlNum(c: Char): Boolean = IsAlpha(c) || IsDigit(c)

	def IsAddOp(c: Char): Boolean = List('+', '-').contains(c)

	def IsMulOp(c: Char): Boolean = List('*', '/').contains(c)

	def IsOrOp(c: Char): Boolean = List('|', '~').contains(c)

	def IsRelOp(c: Char): Boolean = List('=', '#', '<', '>').contains(c)

	def IsWhite(c: Char): Boolean = List(' ', TAB).contains(c)

	def SkipWhite {
		while (IsWhite(Look)) {
			GetChar
		}
	}

	def Fin {
		if (CR == Look) {
			GetChar
			if (LF == Look) {
				GetChar
			}
		}
	}

	def Match(c: Char) {
		if (c == Look) {
			GetChar
		} else {
			Expected("'" + c + "'")
			SkipWhite
		}
	}

	def GetName: Char = {
		if (!IsAlpha(Look)) {
			Expected(Look.toString)
		}
		val result = Look.toUpper
		GetChar
		result
	}

	def GetNum: Char = {
		if (!IsDigit(Look)) {
			Expected("Integer")
		}
		val result = Look
		GetChar
		result
	}

	def Emit(s: String) {
		print(TAB + s)
	}

	def EmitLn(s: String) {
		Emit(s)
		println
	}

	def PostLabel(s: String) {
		println(s + ':')
	}

	def LoadVar(Name: Char) = {
		CheckVar(Name)
		EmitLn("; Load variable to primary register")
		AST_LoadVar(Name)
	}

	def StoreVar(Name: Char) = {
		CheckVar(Name)
		EmitLn("; Store variable from primary register")
		AST_StoreVar(Name)
	}

	def Init {
		GetChar
		SkipWhite
		ClearParams
	}

	def Expression = {
		val ast_load_var = LoadVar(GetName)
		AST_Expression(ast_load_var)
	}

	def Assignment = {
		val Name = GetName
		Match('=')
		val ast_expression = Expression
		val ast_store_var = StoreVar(Name)
		AST_Assignment(Name, ast_expression, ast_store_var)
	}
	
	def Call(c: Char) = {
	  AST_Call(c)
	}
	
	def AssignOrProc = {
	  val Name = GetName
	  TypeOf(Name) match {
	    case ' ' => Undefined(Name)
	    case 'v' => Assignment(Name)
	    case 'p' => CallProc(Name)
	    case  _  => Abort("Identifier " + Name + " Cannot Be Used Here")
	  }
	}

	def DoBlock = {
		var ast_assignment_list = new ArrayBuffer[AST_Assignment](0)
		while ('e' != Look) {
			ast_assignment_list += AssignOrProc
			Fin
		}
		AST_Block(ast_assignment_list)
	}

	def BeginBlock = {
		Match('b')
		Fin
		val ast_block = DoBlock
		Match('e')
		Fin
		ast_block
	}

	def Alloc(c: Char) = {
		AddEntry(c, 'v')
		AST_Allocate(c, 'v')
	}

	def Decl = {
		Match('v')
		val ast_allocate = Alloc(GetName)
		AST_Declaration(ast_allocate)
	}

	def TopDecls = {
		var ast_declaration_list = new ArrayBuffer[AST_Declaration](0)
		while ('b' != Look) {
			Look match {
				case 'v' => ast_declaration_list += Decl
				case 'p' => ast_declaration_list += DoProc
				case 'P' => ast_declaration_list += DoMain
				case  _  => Abort("Unrecognized Keyword " + Look)
			}
			Fin
		}
		ast_declaration_list
	}

	def main(args: Array[String]): Unit = {
		Init
		val ast_declaration_list = TopDecls
		val ast_epilog = Epilog
		println(AST_Program(ast_declaration_list, ast_epilog))
	}
	
	def FormalParam = {
	  val Name = GetName
	  AST_FormalParam(Name)
	}
	
	def FormalList = {
	  val ast_formal_list = new ArrayBuffer[AST_FormalParam](0)
	  Match('(')
	  if (')' != Look) {
	    ast_formal_list += FormalParam
	    while (',' == Look) {
	      Match(',')
	      ast_formal_list += FormalParam
	    }
	  }
	  Match(')')
	  ast_formal_list
	}
	
	def DoProc = {
	  Match('p')
	  val N = GetName
	  val ast_formal_list = FormalList
	  Fin
	  AddEntry(N, 'p')
	  PostLabel(N)
	  val ast_block = BeginBlock
	  ClearParams
	  AST_ProcDecl(N, ast_formal_list, ast_block)
	}
	
	def DoMain = {
	  Match('P')
	  val N = GetName
	  Fin
	  if (InTable(N)) {
	    Duplicate(N)
	  }
	  val p = Prolog
	  val b = BeginBlock
	  AST_Main(p,b)
	}
	
	def Param = {
	  val Name = GetName
	  AST_Param(Name)
	}
	
	def ParamList = {
	  val ast_param_list = new ArrayBuffer[AST_Param](0)
	  Match('(')
	  if (')' != Look) {
	    ast_param_list += Param
	    while (',' == Look) {
	      Match(',')
	      ast_param_list += Param
	    }
	  }
	  Match(')')
	  ast_param_list
	}
	
	def CallProc(Name: Char) = {
	  val ast_param_list = ParamList
	  val ast_call = Call(Name)
	  AST_CallProc(ast_param_list, ast_call)
	}
	
	def ClearParams {
	  for (i <- 0 until Params.length) {
	    Params(i) = 0
	  }
	  NumParams = 0
	}
	
	def ParamNumber(c: Char): Int = {
	  Params(c)
	}
	
	def IsParam(c: Char): Boolean = Params(c) != 0
	
	def AddParam(Name: Char) = {
	  if (IsParam(Name)) {
	    Duplicate(Name)
	  }
	  NumParams += 1
	  Params(Name) = NumParams
	}
	
	def LoadParam(N: Int) {
}